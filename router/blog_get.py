from router.blog_post import required_functionality
from typing import Optional
from fastapi import APIRouter, status, Response, Depends
from enum import Enum

router = APIRouter(
    prefix='/blog',
    tags=['blog']
)

#Pidantic
# @app.get('/blog/all')
# def get_all_blogs():
#     return {'message': 'All blogs provided'}

#default value
# @app.get('/blog/all')
# def get_all_blogs(page = 1, page_size = 10):
#     return {'message' : f'All {page_size} blogs on page {page}'}

#optional parameters
@router.get('/all',
         tags=['comment'], 
         summary="Tóm tắt", 
         description= "Mô tả", 
         response_description= "Danh sách của 1 blog có sẵn")
#req_parameter: dict = Depends(required_functionality): 1 hàm dựa vào 1 hàm khác 
def get_all_blogs(page = 1, page_size: Optional[int] = None, req_parameter: dict = Depends(required_functionality)):
    return {'message' : f'All {page_size} blogs on page {page}', 'req':req_parameter}

@router.get('/{id}/comments/{comment_id}')
def get_comment(id: int, comment_id: int, valid: bool = True, username: Optional[str] = None):
    """Công dụng:
    - **id**jsdkdldk
    - **comment*hhhhh
    """
    return {'message' : f'blog_id {id}, comment_id {comment_id}, valid {valid}, username {username}'}

# @app.post('/hello')
# def index2():
#     return 'Hi'

#Enum
class BlogType(str, Enum):
    short = 'short'
    story = 'story'
    howto = 'howto'

#Tags
@router.get('/type/{type}')
def get_blog_type(type: BlogType, req_parameter: dict = Depends(required_functionality)):
    return {'message' : f'Blog type {type.value}'}

#Status code
# @app.get('/blog/{id}', status_code = status.HTTP_404_NOT_FOUND)
# def get_blog(id: int):
#     if id > 5:
#         return {'error' : f'Blog {id} not found'}
#     else:
#         return {"message": f"Blog with id {id}"}

#Response
@router.get('/{id}', status_code = status.HTTP_200_OK)
def get_blog(id: int, response: Response, req_parameter: dict = Depends(required_functionality)):
    if id > 5:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {'error' : f'Blog {id} not found'}
    else:
        response.status_code = status.HTTP_200_OK
        return {"message": f"Blog with id {id}"}