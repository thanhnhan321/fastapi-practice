from typing import Optional, List, Dict
from fastapi import APIRouter, Query, Path, Body
from fastapi.params import Body
from pydantic import BaseModel

router = APIRouter(
    prefix='/blog',
    tags=['blog']
)

class Image(BaseModel):
    url: str
    alias: str

#Request body
#Read Request body as JSON
class BlogModel(BaseModel):
    title: str
    contect: str
    nb_comments: int
    published: Optional[bool]
    # Pydantic models không bị giới hạn chỉ cho các kiểu dữ liệu đơn giản mà còn có thể mô tả và kiểm tra kiểu dữ liệu của các dữ liệu phức tạp hoặc tùy chỉnh.
    tags: List[str] = str
    # "complex subtypes" thường đề cập đến khả năng sử dụng các kiểu dữ liệu phức tạp, tức là những kiểu dữ liệu được xây dựng từ các kiểu dữ liệu cơ bản hoặc đã có sẵn. 
    metadata: Dict[str, str] = {'key1':'value1'}
    # "complex subtypes"bằng cách custom medel
    image: Optional[Image] = None

@router.post('/new/{id}')
def create_blog(blog: BlogModel, id: int, version: int=1):
    return {
        'id': id,
        'data' : blog,
        'version' : version
        }

#Add title and description
@router.post('/new/{id}/comment/{comment_id}')
def create_comment(blog: BlogModel, id: int, 
        comment_title: int = Query(None,
            title='Title of comment',
            description= 'Some description for comment_title',
            #Biệt danh gán cho tham số truy vấn
            alias='commentTitle',
            #Đánh dấu tham số truy vấn là đã dùng
            deprecated=True
        ),
        #Validators (truyền cứng)
        #content: str = Body('hi how are you')

        #Validators (Yêu cầu 1 giá trị)
        #content: str = Body(...)

        #Validators (Yêu cầu 1 giá trị) và đặt giới hạn độ dài
        content: str = Body(...,
                min_length=10,
                max_length=50,
                #Chấp nhận chuỗi từ a đến z và space
                regex='^[a-z\s]*$'
            ),
            #Khai báo lựa chọn truy vấn nhiều tham số 
            v: Optional[List[str]] = Query(['1.0','2.0','3.0']),
            # 5 < comment_id <= 10
            comment_id: int = Path(..., gt=5, le=10)
    ):
    return {
        'blog': blog,
        'id' : id,
        'comment_title': comment_title,
        'content': content,
        'version': v,
        'comment_id': comment_id
    }

def required_functionality():
    return {'message' : 'Learning FastAPI is important'}

