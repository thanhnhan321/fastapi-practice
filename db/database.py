
#Khai báo CSDL và bảng

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


SQLALCHEMY_DATABASE_URL = "sqlite:///./fastapi-practice.db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
#autocommit=False và autoflush=False được sử dụng để kiểm soát quy trình commit và flush của session, và bind=engine liên kết session với đối tượng engine đã tạo trước đó.
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

#Định nghĩa các bảng trong cơ sở dữ liệu.
Base = declarative_base()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()